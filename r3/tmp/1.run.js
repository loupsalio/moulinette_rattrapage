function reverseArray(array) {
	if(array && Array.isArray(array)) {
		var copy = array.slice();
		var reverseCopy = copy.reverse();
		return reverseCopy;
	}
}

//--------------------------------

var monTab = ["ceci", "est", "un", "test"];
console.log(reverseArray(monTab));
console.log(monTab);
