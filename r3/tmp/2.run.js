function arraySort(array) {
	if(array && Array.isArray(array)) {
		var copy = array.slice();
		copy.sort(function(a,b){ return b-a });
		return copy;
	}
}

//--------------------------------

var myArr = ["test", "ceci", "un", "est"];
console.log(arraySort(myArr));
console.log(myArr);
myArr = [66, -1, 7345, 2];
console.log(arraySort(myArr));
console.log(myArr);
