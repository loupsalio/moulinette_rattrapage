function hasValue(obj, value) {
	if(obj !== null && typeof obj === 'object' && typeof value == "string") {
		var nbr = 0;
		var found = false;
		var foundProperty = '';

		Object.values(obj).map(val => {
			if(val != value && found == false) {
				nbr++;
			}
			else {
				found = true;
			}
		});

		var i = 0;
		Object.keys(obj).map(prop => {
			if(i == nbr && !foundProperty) {
				foundProperty = prop;
			}
			else {
				i++;
			}
		});
		return (foundProperty) ? foundProperty : false;
	}
}

//--------------------------------

var monObj = {
    "status" : "work",
    "name" : "jack"
}

console.log(hasValue(monObj, "work"));
console.log(hasValue(monObj, "test"));
