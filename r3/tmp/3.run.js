function exportObject(obj) {
	if(obj !== null && typeof obj === 'object') {
		var orderedProperty = [];
		var orderedObject = [];

		Object.getOwnPropertyNames(obj).sort().map(prop => {
			orderedProperty.push(prop);
		});

		var i = 0;
		while(i < orderedProperty.length) {
			orderedObject.push(obj[orderedProperty[i]]);
			i++;
		}
		return orderedObject;
	}
}

//--------------------------------

var monObj = {
    "exemple_b": "ceci",
    "exemple_a": "est",
    "exemple_c": "un",
    "exemple_d": "test",
}
console.log(exportObject(monObj));
