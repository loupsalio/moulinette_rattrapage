#!/bin/bash

cat ex_01.js > tmp/1.run.js
cat tests/1.run.js >> tmp/1.run.js
cat ex_02.js > tmp/2.run.js
cat tests/2.run.js >> tmp/2.run.js
cat ex_03.js > tmp/3.run.js
cat tests/3.run.js >> tmp/3.run.js
cat tests/4.prerun.js > tmp/4.run.js
cat ex_04.js >> tmp/4.run.js
cat tests/4.run.js >> tmp/4.run.js
cat ex_06.js > tmp/6.run.js
cat tests/6.run.js >> tmp/6.run.js
cat ex_07.js > tmp/7.run.js
cat tests/7.run.js >> tmp/7.run.js
cat ex_08.js > tmp/8.run.js
cat tests/8.run.js >> tmp/8.run.js

js tmp/1.run.js > results/1.result 2> results/errors
js tmp/2.run.js > results/2.result 2>> results/errors
js tmp/3.run.js > results/3.result 2>> results/errors
time js tmp/4.run.js > results/4.result 
js tmp/6.run.js > results/6.result 2>> results/errors
js tmp/7.run.js > results/7.result 2>> results/errors
js tmp/8.run.js > results/8.result 2>> results/errors
    
echo "---------------------------------";

var=$(diff verif/1.test results/1.result)
if [ ! -z "$var" -a "$var" != " " ]; then
    echo "Exercice 1 : \033[31m FAIL\033[0m";
else
    echo "Exercice 1 : \033[37m OK\033[0m";
fi

var=$(diff verif/2.test results/2.result)
if [ ! -z "$var" -a "$var" != " " ]; then
    echo "Exercice 2 : \033[31m FAIL\033[0m";
else
    echo "Exercice 2 : \033[37m OK\033[0m";
fi

var=$(diff verif/3.test results/3.result)
if [ ! -z "$var" -a "$var" != " " ]; then
    echo "Exercice 3 : \033[31m FAIL\033[0m";
else
    echo "Exercice 3 : \033[37m OK\033[0m";
fi

var=$(diff verif/4.test results/4.result)
if [ ! -z "$var" -a "$var" != " " ]; then
    echo "Exercice 4 : \033[31m FAIL\033[0m";
else
    echo "Exercice 4 : \033[37m OK\033[0m";
fi

var=$(diff verif/6.test results/6.result)
if [ ! -z "$var" -a "$var" != " " ]; then
    echo "Exercice 6 : \033[31m FAIL\033[0m";
else
    echo "Exercice 6 : \033[37m OK\033[0m";
fi

var=$(diff verif/7.test results/7.result)
if [ ! -z "$var" -a "$var" != " " ]; then
    echo "Exercice 7 : \033[31m FAIL\033[0m";
else
    echo "Exercice 7 : \033[37m OK\033[0m";
fi

var=$(diff verif/8.test results/8.result)
if [ ! -z "$var" -a "$var" != " " ]; then
    echo "Exercice 8 : \033[31m FAIL\033[0m";
else
    echo "Exercice 8 : \033[37m OK\033[0m";
fi

echo "---------------------------------";
