function add_text(){
    $("#result_ex_09").css("color", "red");
}

$( document ).ready(function() {
    $("#run_ex_05").click(function(){
        var myDom = document.getElementById("result_ex_05");
        setStyle(myDom, "color", "red");
        if($("#result_ex_05").css("color") == "rgb(255, 0, 0)"){
            $("#result_ex_05").html("Work");
            $("#result_ex_05").css("color", "green");
        }
        else{
            $("#result_ex_05").html("Not Work");
            $("#result_ex_05").css("color", "Red");
        }
    });

    var myDom = document.getElementById("result_ex_09");
    addOneShotListener(myDom, "click", function () { add_text()});
    

    $("#run_ex_09").click(function(){
        if($("#result_ex_09").css("color") == "rgb(255, 0, 0)"){
            $("#result_ex_09").html("Work");
            $("#result_ex_09").css("color", "green");
        }
        else{
            $("#result_ex_09").html("Not Work");
            $("#result_ex_09").css("color", "Red");
        }
    });

    var my_test = document.getElementById("test_ex_10");
    my_clickEvt = new MouseEvent('click', {});
    delegateListener(my_test, "click", function () {   $("#result_ex_10").css("color", "red");});

    $('#run_ex_10_no').click(function(){
        if ($("#result_ex_10").css("color") == "rgb(255, 0, 0)")
        {
            $("#result_ex_10").html("Not Work");
            $("#result_ex_10").css("color", "Red");
        }
    })
});