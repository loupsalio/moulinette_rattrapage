<?php

include('ex_01.php');
include('ex_02.php');
include('ex_03.php');
include('ex_04.php');
include('ex_05.php');
include('ex_06.php');
include('ex_07.php');
include('ex_08.php');
include('ex_09.php');
include('ex_10.php');

// include("functions.php");

echo "isPalindrome tests\n\n";

var_dump(isPalindrome("kayak")); //true
var_dump(isPalindrome("canoe")); //false
var_dump(isPalindrome("abcd-dc ba")); //true
var_dump(isPalindrome("a__________________________________________________________________________________________a")); //true
var_dump(isPalindrome("a______________b.............................c-- .c .............._---b-.__________________a")); //true
var_dump(isPalindrome("a______________b.............................c-- .c .............._---b-._________d_________a")); //false
var_dump(isPalindrome("a_________d____b..........123...................c-- .c ......321........_---b-._________d_________a")); //true
var_dump(isPalindrome("")); //true
var_dump(isPalindrome("a\/a")); //false

echo "----------------------------------------------------------\n";

echo "factorialize tests\n\n";

var_dump(factorialize(0));
var_dump(factorialize(4));
var_dump(factorialize(12));
var_dump(factorialize("test"));
var_dump(factorialize(3.3));
var_dump(factorialize(-12));

echo "----------------------------------------------------------\n";

echo "biggestWord tests\n\n";

var_dump(biggestWord("Hello Pangolin"));
var_dump(biggestWord("      cscs  Hellodddddddddd Pangolin"));

echo "----------------------------------------------------------\n";

echo "biggestInt tests\n\n";

var_dump(biggestInt([12, 54, 87, -34, 23, 1, 0, 3333, 44]));
var_dump(biggestInt([0, 3, 7, 5, 4]));

echo "----------------------------------------------------------\n";

echo "evenElements tests\n\n";

var_dump(evenElements([1, 2, [3, 4], 5, 6])); //[1, [3, 4], 6]

echo "----------------------------------------------------------\n";

echo "myStrRepeat tests\n\n";

var_dump(myStrRepeat("Bar", 0)); //""
var_dump(myStrRepeat("Foo", 4)); //"FooFooFooFoo"
var_dump(myStrRepeat("Foo", -4)); //""

echo "----------------------------------------------------------\n";

echo "myUcWords tests\n\n";

var_dump(myUcWords("hello pangolin Hello world")); //"Hello Pangolin Hello World"
var_dump(myUcWords("")); 
var_dump(myUcWords("hello")); 
var_dump(myUcWords("                      ")); 
 
echo "----------------------------------------------------------\n";

echo "myRot13 tests\n\n";

var_dump(myRot13('pangolin')); //"cnatbyva"
var_dump(myRot13('im back marco!'));
 
echo "----------------------------------------------------------\n";

echo "not tests\n\n";

var_dump(not(false));
var_dump(not(true));
